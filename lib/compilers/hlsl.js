var Compile = require('../base-compiler');

function compileHLSL(info, env) {
    var compiler = new Compile(info, env);

    // TODO: parse user options and correctly die if inapropriate -c options (can't do more than one device, also 1 is required)
    compiler.optionsForFilter = function (filters, outputFilename, userOptions) {
        return ['/Fc', this.filename(outputFilename)];
    };
    return compiler.initialise();
}

module.exports = compileHLSL;
