#pragma comment(lib, "Dbghelp.lib")

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

#include <dbghelp.h>
#include <stdio.h>

#include <assert.h>

#include <string>

// NOT THREAD SAFE
const char* undecorate2(const char* sz_token)
{
    // Can't seem to reliably know if UnDecorateSymbolName fails because the buffer is too short, so we use a
    // 'large enough' buffer (and in the worst case scenario, we return the undecorated name so no big deal)
    static char und_buf[1024];

    if (*sz_token == '\0') // Don't process, otherwise it returns ' ?? ' *shrugs*
        return sz_token; // return as is

    DWORD ret = UnDecorateSymbolName(sz_token, und_buf, 1024, UNDNAME_COMPLETE);

    if (ret == 0) // Failed
        return sz_token; // return as is

    return und_buf;
}

int main(int argc, char *argv[])
{
    std::string cur_token;
    cur_token.reserve(512);

    while (!feof(stdin))
    {
        const char c = getchar();

        if (c == EOF) // End of file (or error, see below)
        {
            assert(feof(stdin)); // Otherwise it's an error and then: TODO Handle
            break;
        }

        // Note on tokenization: for now we add '$' as a token separator, might not be optimal, more testing needed
        if (strchr(" \n\t()\"\'$", c)) // End of token
        {
            // UNDECORATE CURRENT TOKEN
            // PRINT UNDECORATED + THE TOKEN DELIMITATOR
            const char* und_token = undecorate2(cur_token.c_str());

            printf("%s%c", und_token, c);

            // CLEAR THE CURRENT TOKEN
            cur_token.clear();
        }
        else
        {
            cur_token.push_back(c);
        }
    }

    return 0;
}
